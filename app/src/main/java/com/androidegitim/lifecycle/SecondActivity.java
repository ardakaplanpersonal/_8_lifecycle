package com.androidegitim.lifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class SecondActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);

        Log.i("Life Cycle", "SecondActivity - onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("Life Cycle", "SecondActivity - onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("Life Cycle", "SecondActivity - onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("Life Cycle", "SecondActivity - onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i("Life Cycle", "SecondActivity - onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Life Cycle", "SecondActivity - onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i("Life Cycle", "SecondActivity - onRestart");
    }
}
