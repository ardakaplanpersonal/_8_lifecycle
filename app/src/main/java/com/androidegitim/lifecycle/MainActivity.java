package com.androidegitim.lifecycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("Life Cycle", "MainActivity - onCreate");

        findViewById(R.id.main_button_tus).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, SecondActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i("Life Cycle", "MainActivity - onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("Life Cycle", "MainActivity - onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i("Life Cycle", "MainActivity - onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i("Life Cycle", "MainActivity - onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Life Cycle", "MainActivity - onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i("Life Cycle", "MainActivity - onRestart");
    }
}
